import base64, hashlib

class FilterModule(object):

    def filters(self):
        return {'fingerprint': self.md5_fingerprint}

    def md5_fingerprint(self, line):
        key = base64.b64decode(line.strip().split()[1].encode('ascii'))
        fp_plain = hashlib.md5(key).hexdigest()
        return (':').join((a + b for a, b in zip(fp_plain[::2], fp_plain[1::2])))
